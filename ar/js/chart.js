// $scope.chartConfig.series[0].visible = false;
try{
 
  var jsobject = {
    data: [
      110918014828.58,
      123528752302.25,
      132862436591.28,
      155476445448.28,
      161679441287.24,
      158528181698.21,
      164066857164.88,
      164394920835.42,
      170635522619.67,
      198001509593.71,
      193508008534.69,
      179598527860.35,
      145290931115.95,
      163115274339.22,
      167882914787.46,
      171629806274.71,
      177962668216.69,
      192299964988.56,
      196573183415.01
    ],
    data2: [
      2087008757435.94,
      2185699113091.03,
      2000248221526.30,
      2081078272266.13,
      2057214816577.47,
      1902408703619.21,
      1908447533399.00,
      1818765588633.12,
      1844302964673.90,
      9025438669340.85,
      8748845573208.70,
      8425255907997.67,
      7561968365701.89,
      8005422228039.55,
      8327078772596.22,
      8231831016252.88,
      8380254222781.08,
      8977009366691.38,
      9130186589193.75
    ],
    label: [
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      "قائمة أرامكو",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    prctg: [
    ]
    // ] : 
  }

     
var jsobject2 = {
  data: [
    556,
    667,
    772,
    882,
    1130,
    1214,
    1305,
    1395,
    1458,
    1547,
    1716,
    1853,
    1939
  ],
  label: [
    "",
    "MT30 Index</br> Go Live",
    "FTSE الشريحة الاولى",
    "FTSE الشريحة الثانية",
    null,
    "MSCI الشريحة الاولى",
    "FTSE  الشريحة الثالثة",
    "MSCI الشريحة الاولى",
    null,
    "FTSE الشريحة الرابعة",
    null,
    "قائمة أرامكو",
    null
  ]

}
// let sum =jsobject.data.reduce((a, b), function(){ a + b, 0});

jsobject.data.forEach(function(x,i){
  let prctg = (x/jsobject.data2[i])*100;
  jsobject.prctg.push((prctg));

});

Highcharts.setOptions({
  lang: {
    thousandsSep: ',',
    numericSymbols: ['K', 'M','B','T']
  },
  chartIntervals: 10,

});
// debugger;
var chart1x = Highcharts.chart('container1', {
  plotOptions: {
    series: {
      states: {
        inactive: {
          opacity: 1
        },
      }
    }
  },
  exporting: false,
  lang: {
    thousandsSep: ','
  },

  chart: {
    zoomType: false
  },
  title: {
    text: ''
  },

  xAxis: [{

    categories: [
    "مارس - 19",
    "أبريل - 19",
    "مايو - 19",
    "يونيو - 19",
    "يوليو - 19",
    "أغسطس - 19",
    "سبتمبر - 19",
    "أكتوبر - 19",
    "نوفمبر - 19",
    "ديسمبر - 19",
    "يناير - 19",
    "فبراير - 19",
    "مارس - 20",
    "أبريل - 20",
"مايو - 20",
"يونيو - 20",
"يوليو - 20",
"أغسطس - 20",
"سبتمبر - 20"
  ],
    tooltip: {
      crosshairs: false
    },
    labels: {
      align: 'center',
      useHTML: true,
      style: {
        fontSize: '9px',
        paddingTop: '30px',
      }
    }
  }],
  yAxis: [

    { // Primary yAxis
      // min: 0,
      // max: 100,
      min: 0,
      max: Math.max.apply(null,jsobject.prctg),
      tickInterval:(Math.max.apply(null,jsobject.prctg)+1)/5,
      labels: {
        
        format: '{value:,.0f}%',
        style: {
          color: "#7dad14 !important"
        }
      },
      title: {
        text: 'من السوق الكلي  %',
        style: {
          color: "#7dad14 !important"
        },
        formatter: function() {
          // console.log(100*jsobject2.data.slice(-1).pop()/jsobject2.data.reduce((a, b) function(){} a + b, 0))
          return this.value+"%";
        // var percent = (100 * this.y) / (this.series.data[0].y + this.series.data[1].y + this.series.data[2].y);
        // percent = percent.toFixed(1);
        // return 'value: ' + this.y + ', percent: ' + percent + '%';
      }
      },
      opposite: true,

      
    }, { // Secondary yAxis

      title: {
        text: 'ملكية المستثمر الأجنبي المؤهل',
        style: {
          color: "rgb(0, 141, 216) !important"
        }
      },
      labels: {
        // format: '{value:,.0f}',
        style: {
          // color: Highcharts.getOptions().colors[2]
          color: "rgb(0, 141, 216) !important"
        }
      },
    }
  ],

  tooltip: {
    shared: true,
    crosshairs: false,
    useHTML:true,
    opposite: true,
    // pointFormatter: function() {
    formatter: function(){

      var point = this;
      var num;
      return '<table class="tooltipBody" style="margin-top:35px;color:white"><tr><td></td><td class="tooltipDate" style="text-align: right;">'+point.x+'</td></tr><tr><td style="text-align: center;">'+numberWithCommas(point.points[0].y)+'</td><td style="direction:rtl" align="right"><span style="color:' + point.points[0].color + '">\u25CF </span>ملكية المستثمر الأجنبي المؤهل: </td></tr><tr><td style="text-align: ;"></td><td style="direction:rtl" align="right"><span style="color:' + point.points[1].color + '">\u25CF </span>% من السوق الكلي  : %'+(point.points[1].y).toFixed(2)+'</td></tr></table>';
      // if(point.color == "#628ca7"){
      //   // num =  numberWithCommas(point.points.y);
      // }else{
      //   num = (point.options.y).toFixed(2)+"%";
      // }
      // return '<b>' + point.series.name + ': '+num+'</table>  <span style="direction:rtl !important;color:' + point.color + '">\u25CF </span><br/>';
      // return '<b style="direction:rtl !important">' + point.series.name + '</b>';
    }
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 100,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'ملكية المستثمر الأجنبي المؤهل',
    showInLegend: false,
    type: 'column',
    yAxis: 1,

    data: jsobject.data,
    dataLabels: {
      enabled: true,
      inside: true,
      useHTML: true,
      formatter: function () {
        if (this.y > 0)
          if (jsobject.label[this.series.data.indexOf(this.point)])
            return (jsobject.label[this.series.data.indexOf(this.point)]);
      },
      verticalAlign: 'top'
    },
    color: "#628ca7",
  }, {
    name: 'من السوق الكلي  %',
    type: 'spline',
    showInLegend: false,
    data: jsobject.prctg,

    color: Highcharts.getOptions().colors[2]

  }]
}, function (chart) {
  var max = chart.yAxis[0].toPixels(chart.series[0].data[0].y, true);
  $.each(chart.series[0].data, function (i, d) {
    try {
      d.dataLabel.attr({
        y: d.dataLabel.y - 40
      });
    } catch (error) {
    }
  });
});


// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************






var chart2x = Highcharts.chart('container2', {
  plotOptions: {
    series: {
      states: {
        inactive: {
          opacity: 1
        }
      }
    }
  },
  exporting: false,
  lang: {
    thousandsSep: ','
  },

  chart: {
    type: 'column',
    zoomType: false

  },
  title: {
    text: ''
  },

  xAxis: [{
    categories: [
"مارس - 19",
"أبريل - 19",
"مايو - 19",
"يونيو - 19",
"يوليو - 19",
"أغسطس - 19",
"سبتمبر - 19",
"أكتوبر - 19",
"نوفمبر - 19",
"ديسمبر - 19",
"يناير - 19",
"فبراير - 19",
"مارس - 20"
    ],
    tooltip: {
      crosshairs: false
    },
    labels: {
      align: 'center',
      useHTML: true,
      // rotation: -45,
      autoRotation: [0, -45],
      // align:'bottom',
      style: {
        fontSize: '13px',
        paddingTop: '30px',
      }
    }
  }],
  yAxis: [

    // { // Primary yAxis
    //   min: 0,
    //   max: Math.round(100*jsobject2.data.slice(-1).pop()/jsobject2.data.reduce((a, b) function(){} a + b, 0))+3,
    //   tickInterval: Math.round(100*jsobject2.data.slice(-1).pop()/jsobject2.data.reduce((a, b) function(){} a + b, 0)+3)/5,
    //   labels: {
    //     format: '',
    //     style: {
    //       color: "rgb(0, 141, 216) !important"
    //     },
    //     formatter: function() {
    //         let total = jsobject2.data.reduce((a, b) function(){} a + b, 0);
    //         // console.log(100*jsobject2.data.slice(-1).pop()/jsobject2.data.reduce((a, b) function(){} a + b, 0))
    //         return this.value+"%";
    //       // var percent = (100 * this.y) / (this.series.data[0].y + this.series.data[1].y + this.series.data[2].y);
    //       // percent = percent.toFixed(1);
    //       // return 'value: ' + this.y + ', percent: ' + percent + '%';
    //     }
    //   },
    //   title: {
    //     text: '% of Total Registered QFI',
    //     style: {
    //       color: "rgb(0, 141, 216) !important"
    //     }
    //   },
    //   opposite: true,        
    // },
    { // Secondary yAxis
      
      title: {
        text: 'عدد المستثمرين الأجانب المؤهلين المسجلين',
        style: {
          color: "rgb(0, 141, 216) !important"
        }
      },
      labels: {
        format: '{value:,.0f}',
        style: {
          color: "rgb(0, 141, 216) !important"
        }
      } 
    }
  ],

  tooltip: {
    crosshairs: false,
    pointFormatter: function() {
      var point = this;
      var num;
      num = numberWithCommas(point.options.y);
      return '<span style="color:' + point.color + '">\u25CF </span><b>' + point.series.name + ': '+num+'</b><br/>';
    },
      // borderWidth: 0,
      backgroundColor: "rgba(255,255,255,0)",
      // borderRadius: 0,
      // shadow: false,
          pointFormatter: function() {
      var point = this;
      var num;
      num = numberWithCommas(point.options.y);
      return '<span style="color:' + point.color + '">\u25CF </span><b>' + point.series.name + ': '+num+'</b><br/>';
    },
      useHTML: true,
      formatter: function () {
        // return this;
        // let percentX = (100*this.y/jsobject2.data.reduce((a, b) function(){} a + b, 0)).toFixed(2);
        return '<table class="tooltipBody" style="color:white;margin-top:45px"><tr><td align="right" style="direction:rtl !important"><small class="tooltipDate">'+this.x+'</small></td></tr><tr><td style="direction:rtl !important"><span style="color:' + this.color + '">\u25CF </span> عدد المستثمرين الأجانب المؤهلين المسجلين: '+this.y+'</td></tr></table>';
      },
      style: {
        // 'color': 'pink',
        'z-index': '9999'
      },
      positioner: function(labelWidth, labelHeight, point) {
        var x;
        if(labelWidth > point.plotX){
          x = point.plotX;
        }else{
          x = point.plotX -150;
        }

        var tooltipX = x;
        var tooltipY = point.plotY+40;
        return {
            x: tooltipX,
            y: tooltipY
        };
    }
  },
 
  series: [{
    name: 'عدد المستثمرين الأجانب المؤهلين المسجلين',
    // visible: false,
    // showInLegend: "false"
    showInLegend: false,
    type: 'column',
    yAxis: 0,

    data: jsobject2.data,
    dataLabels: {
      enabled: true,
      allowOverlap:false,
      align: 'center',
      color: '#FFFFFF',
      overflow:"none",
      inside: true,
      crop: false,
      useHTML: true,
      zIndex: 1,
      formatter: function () {
        
        if (this.y > 0)
          if (jsobject2.label[this.series.data.indexOf(this.point)])
            return jsobject2.label[this.series.data.indexOf(this.point)];
      },
      verticalAlign: 'top'

    },
    color: "#628ca7",


  }]
}, function (chart) {
  $.each(chart.series[0].data, function (i, d) {
    try {
      d.dataLabel.attr({
        y: d.dataLabel.y - 40
      });
    } catch (error) {
      // console.log(error)
    }
  });
});

}catch(e){
  
}
$(window).resize();
function numberWithCommas(x) {
  x = parseInt(x)
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}