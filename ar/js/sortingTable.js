
/* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
   // return '<table cellpadding="5" cellspacing="0" border="0" class="fulldatalist" style="padding-left:50px;">'+
//        '<tr>'+
//            '<td>Full name:</td>'+
//            '<td>'+d.name+'</td>'+
//        '</tr>'+
//        '<tr>'+
//            '<td>Extension number:</td>'+
//            '<td>'+d.extn+'</td>'+
//        '</tr>'+
//        '<tr>'+
//            '<td>Extra info:</td>'+
//            '<td>And any further details here (images etc)...</td>'+
//        '</tr>'+
//    '</table>';
	
	return '<div class="fulldatalist">'+
		 '<div class="Listitemleft">'+d.address1+'</div>'+
		 '<div class="Listitemright">'+d.address2+'</div>'+
		 '<div class="clear"></div>'+
	'</div>';
	
	
	
}
  
$(document).ready(function() {
    var table = $('#example').DataTable( {
        "ajax": "ajax/data/objects.txt",
        "columns": [
   
            { "data": "logo", render:getImg },
            { "data": "name" ,"className":'moreinfo'},
            { "data": "value" },
            { "data": "volume" },
            { "data": "trades" },
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
        ],
        "bPaginate": false,
        "searching":false,
		"info":     false,
        "scrollY":  "600px",
        "scrollX": true,
        "order": [[0, 'asc']],
        responsive: true,
        "initComplete": function(settings, json) {
            $('body').find('.dataTables_scrollBody').addClass("scrollbarx");
            // $(".xsort").click();
        },
    } );
	
     
    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control, td.moreinfo', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
		
		
    } );

    function getImg(data) {
        return '<img width="100%" src="'+data+'" />';
    }
} );