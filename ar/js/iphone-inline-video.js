var enableInlineVideo=(function(){'use strict';function intervalometer(cb,request,cancel,requestParameter){var requestId;var previousLoopTime;function loop(now){requestId=request(loop,requestParameter);cb(now-(previousLoopTime||now));previousLoopTime=now;}
return{start:function start(){if(!requestId){loop(0);}},stop:function stop(){cancel(requestId);requestId=null;previousLoopTime=0;}};}
function frameIntervalometer(cb){return intervalometer(cb,requestAnimationFrame,cancelAnimationFrame);}
function preventEvent(element,eventName,test){function handler(e){if(!test||test(element,eventName)){e.stopImmediatePropagation();}}
element.addEventListener(eventName,handler);return handler;}
function proxyProperty(object,propertyName,sourceObject,copyFirst){function get(){return sourceObject[propertyName];}
function set(value){sourceObject[propertyName]=value;}
if(copyFirst){set(object[propertyName]);}
Object.defineProperty(object,propertyName,{get:get,set:set});}
function proxyEvent(object,eventName,sourceObject){sourceObject.addEventListener(eventName,function(){return object.dispatchEvent(new Event(eventName));});}
function dispatchEventAsync(element,type){Promise.resolve().then(function(){element.dispatchEvent(new Event(type));});}
var iOS8or9=typeof document==='object'&&'object-fit'in document.head.style&&!matchMedia('(-webkit-video-playable-inline)').matches;var IIV='bfred-it:iphone-inline-video';var IIVEvent='bfred-it:iphone-inline-video:event';var IIVPlay='bfred-it:iphone-inline-video:nativeplay';var IIVPause='bfred-it:iphone-inline-video:nativepause';function getAudioFromVideo(video){var audio=new Audio();proxyEvent(video,'play',audio);proxyEvent(video,'playing',audio);proxyEvent(video,'pause',audio);audio.crossOrigin=video.crossOrigin;audio.src=video.src||video.currentSrc||'data:';return audio;}
var lastRequests=[];var requestIndex=0;var lastTimeupdateEvent;function setTime(video,time,rememberOnly){if((lastTimeupdateEvent||0)+200<Date.now()){video[IIVEvent]=true;lastTimeupdateEvent=Date.now();}
if(!rememberOnly){video.currentTime=time;}
lastRequests[++requestIndex%3]=time*100|0/100;}
function isPlayerEnded(player){return player.driver.currentTime>=player.video.duration;}
function update(timeDiff){var player=this;if(player.video.readyState>=player.video.HAVE_FUTURE_DATA){if(!player.hasAudio){player.driver.currentTime=player.video.currentTime+((timeDiff*player.video.playbackRate)/1000);if(player.video.loop&&isPlayerEnded(player)){player.driver.currentTime=0;}}
setTime(player.video,player.driver.currentTime);}else if(player.video.networkState===player.video.NETWORK_IDLE&&player.video.buffered.length===0){player.video.load();}
if(player.video.ended){delete player.video[IIVEvent];player.video.pause(true);}}
function play(){var video=this;var player=video[IIV];if(video.webkitDisplayingFullscreen){video[IIVPlay]();return;}
if(player.driver.src!=='data:'&&player.driver.src!==video.src){setTime(video,0,true);player.driver.src=video.src;}
if(!video.paused){return;}
player.paused=false;if(video.buffered.length===0){video.load();}
player.driver.play();player.updater.start();if(!player.hasAudio){dispatchEventAsync(video,'play');if(player.video.readyState>=player.video.HAVE_ENOUGH_DATA){dispatchEventAsync(video,'playing');}}}
function pause(forceEvents){var video=this;var player=video[IIV];player.driver.pause();player.updater.stop();if(video.webkitDisplayingFullscreen){video[IIVPause]();}
if(player.paused&&!forceEvents){return;}
player.paused=true;if(!player.hasAudio){dispatchEventAsync(video,'pause');}
if(video.ended&&!video.webkitDisplayingFullscreen){video[IIVEvent]=true;dispatchEventAsync(video,'ended');}}
function addPlayer(video,hasAudio){var player={};video[IIV]=player;player.paused=true;player.hasAudio=hasAudio;player.video=video;player.updater=frameIntervalometer(update.bind(player));if(hasAudio){player.driver=getAudioFromVideo(video);}else{video.addEventListener('canplay',function(){if(!video.paused){dispatchEventAsync(video,'playing');}});player.driver={src:video.src||video.currentSrc||'data:',muted:true,paused:true,pause:function(){player.driver.paused=true;},play:function(){player.driver.paused=false;if(isPlayerEnded(player)){setTime(video,0);}},get ended(){return isPlayerEnded(player);}};}
video.addEventListener('emptied',function(){var wasEmpty=!player.driver.src||player.driver.src==='data:';if(player.driver.src&&player.driver.src!==video.src){setTime(video,0,true);player.driver.src=video.src;if(wasEmpty||(!hasAudio&&video.autoplay)){player.driver.play();}else{player.updater.stop();}}},false);video.addEventListener('webkitbeginfullscreen',function(){if(!video.paused){video.pause();video[IIVPlay]();}else if(hasAudio&&player.driver.buffered.length===0){player.driver.load();}});if(hasAudio){video.addEventListener('webkitendfullscreen',function(){player.driver.currentTime=video.currentTime;});video.addEventListener('seeking',function(){if(lastRequests.indexOf(video.currentTime*100|0/100)<0){player.driver.currentTime=video.currentTime;}});}}
function preventWithPropOrFullscreen(el){var isAllowed=el[IIVEvent];delete el[IIVEvent];return!el.webkitDisplayingFullscreen&&!isAllowed;}
function overloadAPI(video){var player=video[IIV];video[IIVPlay]=video.play;video[IIVPause]=video.pause;video.play=play;video.pause=pause;proxyProperty(video,'paused',player.driver);proxyProperty(video,'muted',player.driver,true);proxyProperty(video,'playbackRate',player.driver,true);proxyProperty(video,'ended',player.driver);proxyProperty(video,'loop',player.driver,true);preventEvent(video,'seeking',function(el){return!el.webkitDisplayingFullscreen;});preventEvent(video,'seeked',function(el){return!el.webkitDisplayingFullscreen;});preventEvent(video,'timeupdate',preventWithPropOrFullscreen);preventEvent(video,'ended',preventWithPropOrFullscreen);}
function enableInlineVideo(video,opts){if(opts===void 0)opts={};if(video[IIV]){return;}
if(!opts.everywhere){if(!iOS8or9){return;}
if(!(opts.iPad||opts.ipad?/iPhone|iPod|iPad/:/iPhone|iPod/).test(navigator.userAgent)){return;}}
video.pause();var willAutoplay=video.autoplay;video.autoplay=false;addPlayer(video,!video.muted);overloadAPI(video);video.classList.add('IIV');if(video.muted&&willAutoplay){video.play();video.addEventListener('playing',function restoreAutoplay(){video.autoplay=true;video.removeEventListener('playing',restoreAutoplay);});}
if(!/iPhone|iPod|iPad/.test(navigator.platform)){console.warn('iphone-inline-video is not guaranteed to work in emulated environments');}}
return enableInlineVideo;}());