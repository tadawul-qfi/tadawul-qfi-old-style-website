{
  "data": [
    {
      "id": "1",
      "name": "Al Rajhi Capital",
      "position": "2,546,123",
      "salary": "4,546",
      "address1": "Al Rajhi Capital, Head Office, King Fahd Road<br>P.O. Box 5561, Riyadh 11432,<br>Kingdom of Saudi Arabia",
      "office": "4,546",
      "address2": "Customer Service: 920005856 <br>Fax: +966 11 211 9299 <br>customerservice@alrajhi-capital.com <br><a href='https://www.alrajhi-capital.com/'>www.alrajhi-capital.com</a>",
      "logo":"img/sorting-logos/alrajhi.png"
    },
    {
      "id": "2",
      "name": "Al-Khair Capital Saudi Arabia",
      "position": "1,546,123",
      "salary": "82,546",
      "address1": "Al-Khair Capital Saudi Arabia",
      "office": "82,546",
      "address2": "Al-Khair Capital Saudi Arabia",
      "logo":"img/sorting-logos/alkhair.png"
    },
    {
      "id": "3",
      "name": "AlBilad Investment Co.",
      "position": "2,596,826",
      "salary": "4,546",
      "address1": "AlBilad Investment Co.",
      "office": "4,546",
      "address2": "AlBilad Investment Co.",
      "logo":"img/sorting-logos/albilad.png"
    },
    {
      "id": "4",
      "name": "AlJazira Capital Company",
      "position": "4,546,123",
      "salary": "1,867",
      "address1": "AlJazira Capital Company",
      "office": "1,867",
      "address2": "AlJazira Capital Company",
      "logo":"img/sorting-logos/aljazira.png"
    },
    {
      "id": "5",
      "name": "Alawwal Invest",
      "position": "2,546,123",
      "salary": "1,867",
      "address1": "Alawwal Invest",
      "office": "1,867",
      "address2": "Alawwal Invest",
      "logo":"img/sorting-logos/alawwal.png"
    },
    {
      "id": "6",
      "name": "Alinma Investment Company",
      "position": "1,546,123",
      "salary": "4,546",
      "address1": "Alinma Investment Company",
      "office": "4,546",
      "address2": "Alinma Investment Company",
      "logo":"img/sorting-logos/alinma.png"
    },
    {
      "id": "7",
      "name": "Alistithmar For <br>Financial Securities and <br>Brokerage Company",
      "position": "2,596,826",
      "salary": "1,867",
      "address1": "Alistithmar",
      "office": "1,867",
      "address2": "Alistithmar",
      "logo":"img/sorting-logos/alistithmar.png"
    },
    {
      "id": "8",
      "name": "Alnefaie Investment Group",
      "position": "4,546,123",
      "salary": "4,546",
      "address1": "Alnefaie Investment Group",
      "office": "4,546",
      "address2": "Alnefaie Investment Group",
      "logo":"img/sorting-logos/alnefaie.png"
    }
    
  ]
}