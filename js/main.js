$(function(){

// Iterate over each select element
$('select').each(function () {

    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    // Hides the select element
    $this.addClass('s-hidden');

    // Wrap the select element in a div
    $this.wrap('<div class="select"></div>');

    // Insert a styled div to sit over the top of the hidden select element
    $this.after('<div class="styledSelect"></div>');

    // Cache the styled div
    var $styledSelect = $this.next('div.styledSelect');

    // Show the first select option in the styled div
    $styledSelect.text($this.children('option').eq(0).text());

    // Insert an unordered list after the styled div and also cache the list
    var $list = $('<ul />', {
        'class': 'options'
    }).insertAfter($styledSelect);

    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children('li');

    // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
     $styledSelect.click(function (e) {
         e.stopPropagation();
		 console.log("heyx");
		 console.log($(this).next('ul.options'))
         $('div.styledSelect.active').each(function () {
		 console.log("heyzx");

             $(this).removeClass('active').next('ul.options').hide();
         });
         $(this).toggleClass('active').next('ul.options').toggle();
     });

    // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option
     $listItems.click(function (e) {
         e.stopPropagation();
         console.log("hey")
         $styledSelect.text($(this).text()).removeClass('active');
         $this.val($(this).attr('rel'));
         $list.hide();
         // alert($this.val()); Uncomment this for demonstration! //
     });

    // Hides the unordered list when clicking outside of it
    $(document).click(function () {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});


	//On Scroll Functionality

	$(window).scroll( function()  {
		var windowTop = $(window).scrollTop();
		windowTop > 80 ? $('nav').addClass('navShadow') : $('nav').removeClass('navShadow');
		windowTop > 80 ? $('ul').css('top','100px') : $('ul').css('top','50px');
	});
	
	
	//Click Logo To Scroll To T	op
	$('#logo').on('click', function()  {
		$('html,body').animate({
			scrollTop: 0
		},100);
	});
	
	//Smooth Scrolling Using Navigation Menu
	// $(".navShadow").find('a[href*="#"]').on('click', function(e){
	// 	$('html,body').animate({
	// 		scrollTop: $($(this).attr('href')).offset().top - 50
	// 	},100);
	// 	e.preventDefault();
	// });
	
	//Toggle Menu
	$('#menu-toggle').on('click', function()  {
		$('#menu-toggle').toggleClass('closeMenu');
		$('ul').toggleClass('showMenu');
		
		$('li').on('click', function()  {
			$('ul').removeClass('showMenu');
			$('#menu-toggle').removeClass('closeMenu');
		});
	});
	
	
	






  /** change value here to adjust parallax level */
  var parallax = -0.9;

  var $bg_images = $(".wp-block-cover-image");
  var offset_tops = [];
  $bg_images.each(function(i, el) {
    offset_tops.push($(el).offset().top);
  });

  $(window).scroll(function() {
    var dy = $(this).scrollTop();
    $bg_images.each(function(i, el) {
      var ot = offset_tops[i];
      $(el).css("background-position", "50% " + (dy - ot) * parallax + "px");
    });
  });



  $(".logoEvent").on('click',function(){
   var t = $(this);
    setTimeout(function()  {
        gotoY( t.offset().top)

	  }, 200);
    $(".logoEvent").removeClass('active');
  });

	
  $(".menuBG").on('click',function(){
    $(".containerNav").fadeIn('animated fadIn');
  });

  $("#signUpThankU").on('click',function(){
    $(".signupbg").hide();
    $(".signupbgTankU").fadeIn();
    $(".signupbgTankU").addClass('animated fadeIn');
  });

  $(".closeMenu").on('click',function(){
    $(".containerNav").fadeOut('animated fadeOut');
  });
	
	
	
	
	
  $(".videoPopUpBTN").on('click',function(){
	$(".videoPopBG").fadeIn('animated fadIn');
	// $('.videoMob')[0].play();
  });

  $(".videoClose").on('click',function(){
	$(".videoPopBG").fadeOut('animated fadeOut');
	// $('.videoMob')[0].pause();
	
  });

	
	


  function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
 }

        $(".watchVideoBG").on('click',function(){
        // mainheadBG
        // headvideo_Thumb
//         animateCSS('.mainheadBG', 'fadeOutLeft');
//         animateCSS('.headvideo_Thumb', 'fadeOut',function(){

//             $(".headvideo").show();
//             $(".playerCover").hide();
//         animateCSS('.headvideo', 'fadeIn');
// $(".playpause").click();
			$(".fullscreenVideo").fadeIn("animated fadeIn");
			$(".video2").click();

        });
        
        // animateCSS('.headvideo_Thumb', 'bounceOutRight');
    // });






	$("#sec01Slide").owlCarousel({
			loop:false,
			margin:20,
			nav:true,
			responsive:{
				0:{
					items:1
				},
				500:{
					items:3
				},
				1000:{
					items:3
				}
			}
    });

	var slide2 = [
		"Tadawul is the largest and most liquid equity market by capitalization in MENA, the third largest exchange among emerging market peers and one of the top ten stock exchanges globally.",
		"Tadawul accounts for 76% of the market capitalization in the Middle East and North Africa (MENA) region.",
		"The listing and commencement of trading of Aramco’s shares occurred within four days of the close of the subscription period – a record time to market, even amongst the most developed global stock exchanges.",
		"Tadawul, through its depository center Edaa, was able to deposit all subscribed shares in the Saudi Aramco IPO into shareholders' accounts in a record-breaking 18 hours following the allocation announcement.",
		"According to the World Economic Forum’s 2019 global competitiveness report, Saudi Arabia ranked second in shareholder governance of 141 countries analyzed.",
		"As part of Vision 2030, by 2030 Saudi Arabia is targeting an increase in foreign direct investment from 3.8% to 5.7% of GDP; an increase in the private sector’s contribution from 40% to 65% of GDP and SME contribution from 20% to 35% of GDP; and an increase in the share of non-oil exports in non-oil GDP from 16% to 50%.",
		"Saudi Arabia is a market comparable in size to Russia and South Africa.",
		"Tadawul’s inclusion by MSCI marked the fastest progression from the index watch list to Emerging Market status of any market in the history of the index."
	];
	for(let i = 0; i<= slide2.length-1;i++){
		$("#sec02Slide").append('<div class="item repaleri admaboli"><p class="text-left">'+slide2[i]+'</p></div>');
	}

	$("#sec02Slide").owlCarousel({
			loop:true,
			margin:0,
			nav:true,
			navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>prev','<i class="fa fa-angle-right" aria-hidden="true"></i>next'],
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
    });

	// $()($("#sec02Slide .owl-dots").width())
	$("#sec02Slide .owl-nav .owl-prev").css('right',$("#sec02Slide .owl-dots").width()+84 )
	
		$("#sec03Slide").owlCarousel({
			loop:true,
			margin:20,
			nav:true,
			responsive:{
				0:{
					items:1,
					stagePadding: 20
				},
				600:{
					items:2,
					stagePadding: 30
				},
				1000:{
					items:3
				}
			}
    });

	$('.owl-dot').each(function(){
		$(this).children('span').text($(this).index()+1);
	});
	


$('.video2').click(function () {
	// if($(this).children('video').get(0).paused){ 
	// 	$(this).children(".video").get(0).play();   
	// 	$(this).children(".playpause").fadeOut();
	// }else{       
	// 	$(this).children(".video").get(0).pause();
	// 	$(this).children(".playpause").fadeIn();
	// }

	var video = $('.video2');

console.log(video[0].paused)
	if(video[0].paused){ 
		video[0].play();   
		$(".playpause2").fadeOut();
	}else{       
		video[0].pause();
		$(".playpause2").fadeIn();
	}
	// console.log(this.pause ? "hey" : "heyhey")
});






// Updated code event for  CTA20 - 15
	$(".TpopUpBG").hide();
	$(".HistoryBG ul li").removeClass("active");
	$(".cEvent").on('click',function(){
		$(".TpopUpBG").fadeOut("animated fadeOut");
		$(".HistoryBG ul li").removeClass("active");
		$(this).parent().addClass("active");
		$("."+$(this).attr('data-id')+".TpopUpBG").fadeIn("animated fadeInTop");
		gotoY( $(this).parent().offset().top);
	});




	$(".closeP").click(function(){
		$(".TpopUpBG").fadeOut("animated fadeOut");
		$(".HistoryBG ul li").removeClass("active");
	});	

	$(".searchBTN").click(function(){
		var query = $("#query").val();
		window.open('https://www.tadawul.com.sa/wps/portal/tadawul/home/search/!ut/p/z1/04_Sj9CPykssy0xPLMnMz0vMAfIjo8zi_Tx8nD0MLIy8DTyMXAwczVy9vV2cTY0M_I30I4EKzBEKLEwtDIAKPFxcjV0DjDx9zPTDCSkoyE7zBAAsWd6S/?query='+query);
		$(".closePop").click();
	});	

	$(".closePop").click(function(){
		$(".srachBG").fadeOut("animated fadeIn");
	});
	
	$(".srachBG").hide();
	$("#search").click(function(){
		$(".srachBG").fadeIn("animated fadeIn");
	});	

	$(".videoClose").click(function(){
		//$(".srachBG").fadeOut("animated fadeIn");
		$(".fullscreenVideo").fadeOut("animated fadeIn");
		$(".video2")[0].pause();
	});	


	function gotoY(y){
		if(isMobile())
		$('html, body').animate({
			scrollTop: y-70
		}, 500);
	 }
	 function isMobile(){
		var isMobile = false; //initiate as false
		// device detection
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
			|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			isMobile = true;
		}
		return isMobile;
	 }

	//  $('#nav-tab a').click(function (e) {
	// 	e.preventDefault();
	// 	$(this).tab('show');
		
	// 	var checked = $(this).find('input').prop('checked');
	// 	$(this).find('input').prop('checked', !checked);
	// });
	$("#genInfo2").on('click',function(){
		gotoY( $('.genInfoTabContBG').parent().offset().top);
	});

})

		$(function(){
		  $("video").lazy();
		  $(".move").each(function(){
			var speed = $(this).data("speed");
		  });


		$(".nav-tabs a").click(function(){
			$(this).tab('show');
		});

		if ($(window).width() < 1151) {
		  $(".move").inertiaScroll({
		  });

		} else {
		  $(".move").inertiaScroll({
			parent: $(".wrapp")
		  });
		}

		$('#newsTickerLTR').breakingNews();
		$('#newsTickerRTL').breakingNews({
			direction: 'rtl'
		});

		});  



! function (p) {
    "use strict";
    p.breakingNews = function (e, t) {
        var i = {
                effect: "scroll",
                direction: "ltr",
                height: 40,
                fontSize: "default",
                themeColor: "default",
                background: "default",
                borderWidth: 1,
                radius: 2,
                source: "html",
                rss2jsonApiKey: "",
                play: !0,
                delayTimer: 4e3,
                scrollSpeed: 2,
                stopOnHover: !0,
                position: "auto",
                zIndex: 99999
            },
            a = this;
        a.settings = {}, a._element = p(e), a._label = a._element.children(".bn-label"), a._news = a._element.children(".bn-news"), a._ul = a._news.children("ul"), a._li = a._ul.children("li"), a._controls = a._element.children(".bn-controls"), a._prev = a._controls.find(".bn-prev").parent(), a._action = a._controls.find(".bn-action").parent(), a._next = a._controls.find(".bn-next").parent(), a._pause = !1, a._controlsIsActive = !0, a._totalNews = a._ul.children("li").length, a._activeNews = 0, a._interval = !1, a._frameId = null;
        var o = function () {
                if (0 < a._label.length && ("rtl" == a.settings.direction ? a._news.css({
                        right: a._label.outerWidth()
                    }) : a._news.css({
                        left: a._label.outerWidth()
                    })), 0 < a._controls.length) {
                    var e = a._controls.outerWidth();
                    "rtl" == a.settings.direction ? a._news.css({
                        left: e
                    }) : a._news.css({
                        right: e
                    })
                }
                if ("scroll" === a.settings.effect) {
                    var t = 0;
                    a._li.each(function () {
                        t += p(this).outerWidth()
                    }), t += 50, a._ul.css({
                        width: t
                    })
                }
            },
            s = function () {
                var l = new XMLHttpRequest;
                l.onreadystatechange = function () {
                    if (4 == l.readyState && 200 == l.status) {
                        var e = JSON.parse(l.responseText),
                            t = "",
                            i = "";
                        switch (a.settings.source.showingField) {
                            case "title":
                                i = "title";
                                break;
                            case "description":
                                i = "description";
                                break;
                            case "link":
                                i = "link";
                                break;
                            default:
                                i = "title"
                        }
                        var s = "";
                        void 0 !== a.settings.source.seperator && void 0 !== typeof a.settings.source.seperator && (s = a.settings.source.seperator);
                        for (var n = 0; n < e.items.length; n++) a.settings.source.linkEnabled ? t += '<li><a target="' + a.settings.source.target + '" href="' + e.items[n].link + '">' + s + e.items[n][i] + "</a></li>" : t += "<li><a>" + s + e.items[n][i] + "</a></li>";
                        a._ul.empty().append(t), a._li = a._ul.children("li"), a._totalNews = a._ul.children("li").length, o(), "scroll" != a.settings.effect && d(), a._li.find(".bn-seperator").css({
                            height: a.settings.height - 2 * a.settings.borderWidth
                        }), f()
                    }
                }, l.open("GET", "https://api.rss2json.com/v1/api.json?rss_url=" + a.settings.source.url + "&count=" + a.settings.source.limit + "&api_key=" + a.settings.source.rss2jsonApiKey, !0), l.send()
            },
            n = function () {
                p.getJSON(a.settings.source.url, function (e) {
                    var t = "",
                        i = "";
                    i = "undefined" === a.settings.source.showingField ? "title" : a.settings.source.showingField;
                    var s = "";
                    void 0 !== a.settings.source.seperator && void 0 !== typeof a.settings.source.seperator && (s = a.settings.source.seperator);
                    for (var n = 0; n < e.length && !(n >= a.settings.source.limit); n++) a.settings.source.linkEnabled ? t += '<li><a target="' + a.settings.source.target + '" href="' + e[n].link + '">' + s + e[n][i] + "</a></li>" : t += "<li><a>" + s + e[n][i] + "</a></li>", "undefined" === e[n][i] && console.log('"' + i + '" does not exist in this json.');
                    a._ul.empty().append(t), a._li = a._ul.children("li"), a._totalNews = a._ul.children("li").length, o(), "scroll" != a.settings.effect && d(), a._li.find(".bn-seperator").css({
                        height: a.settings.height - 2 * a.settings.borderWidth
                    }), f()
                })
            },
            l = function () {
                var e = parseFloat(a._ul.css("marginLeft"));
                e -= a.settings.scrollSpeed / 2, a._ul.css({
                    marginLeft: e
                }), e <= -a._ul.find("li:first-child").outerWidth() && (a._ul.find("li:first-child").insertAfter(a._ul.find("li:last-child")), a._ul.css({
                    marginLeft: 0
                })), !1 === a._pause && (a._frameId = requestAnimationFrame(l), window.requestAnimationFrame && a._frameId || setTimeout(l, 16))
            },
            r = function () {
                var e = parseFloat(a._ul.css("marginRight"));
                e -= a.settings.scrollSpeed / 2, a._ul.css({
                    marginRight: e
                }), e <= -a._ul.find("li:first-child").outerWidth() && (a._ul.find("li:first-child").insertAfter(a._ul.find("li:last-child")), a._ul.css({
                    marginRight: 0
                })), !1 === a._pause && (a._frameId = requestAnimationFrame(r)), window.requestAnimationFrame && a._frameId || setTimeout(r, 16)
            },
            c = function () {
                "rtl" === a.settings.direction ? a._ul.stop().animate({
                    marginRight: -a._ul.find("li:first-child").outerWidth()
                }, 300, function () {
                    a._ul.find("li:first-child").insertAfter(a._ul.find("li:last-child")), a._ul.css({
                        marginRight: 0
                    }), a._controlsIsActive = !0
                }) : a._ul.stop().animate({
                    marginLeft: -a._ul.find("li:first-child").outerWidth()
                }, 300, function () {
                    a._ul.find("li:first-child").insertAfter(a._ul.find("li:last-child")), a._ul.css({
                        marginLeft: 0
                    }), a._controlsIsActive = !0
                })
            },
            u = function () {
                "rtl" === a.settings.direction ? (0 <= parseInt(a._ul.css("marginRight"), 10) && (a._ul.css({
                    "margin-right": -a._ul.find("li:last-child").outerWidth()
                }), a._ul.find("li:last-child").insertBefore(a._ul.find("li:first-child"))), a._ul.stop().animate({
                    marginRight: 0
                }, 300, function () {
                    a._controlsIsActive = !0
                })) : (0 <= parseInt(a._ul.css("marginLeft"), 10) && (a._ul.css({
                    "margin-left": -a._ul.find("li:last-child").outerWidth()
                }), a._ul.find("li:last-child").insertBefore(a._ul.find("li:first-child"))), a._ul.stop().animate({
                    marginLeft: 0
                }, 300, function () {
                    a._controlsIsActive = !0
                }))
            },
            d = function () {
                switch (a._controlsIsActive = !0, a.settings.effect) {
                    case "typography":
                        a._ul.find("li").hide(), a._ul.find("li").eq(a._activeNews).width(30).show(), a._ul.find("li").eq(a._activeNews).animate({
                            width: "100%",
                            opacity: 1
                        }, 1500);
                        break;
                    case "fade":
                        a._ul.find("li").hide(), a._ul.find("li").eq(a._activeNews).fadeIn();
                        break;
                    case "slide-down":
                        a._totalNews <= 1 ? a._ul.find("li").animate({
                            top: 30,
                            opacity: 0
                        }, 300, function () {
                            p(this).css({
                                top: -30,
                                opacity: 0,
                                display: "block"
                            }), p(this).animate({
                                top: 0,
                                opacity: 1
                            }, 300)
                        }) : (a._ul.find("li:visible").animate({
                            top: 30,
                            opacity: 0
                        }, 300, function () {
                            p(this).hide()
                        }), a._ul.find("li").eq(a._activeNews).css({
                            top: -30,
                            opacity: 0
                        }).show(), a._ul.find("li").eq(a._activeNews).animate({
                            top: 0,
                            opacity: 1
                        }, 300));
                        break;
                    case "slide-up":
                        a._totalNews <= 1 ? a._ul.find("li").animate({
                            top: -30,
                            opacity: 0
                        }, 300, function () {
                            p(this).css({
                                top: 30,
                                opacity: 0,
                                display: "block"
                            }), p(this).animate({
                                top: 0,
                                opacity: 1
                            }, 300)
                        }) : (a._ul.find("li:visible").animate({
                            top: -30,
                            opacity: 0
                        }, 300, function () {
                            p(this).hide()
                        }), a._ul.find("li").eq(a._activeNews).css({
                            top: 30,
                            opacity: 0
                        }).show(), a._ul.find("li").eq(a._activeNews).animate({
                            top: 0,
                            opacity: 1
                        }, 300));
                        break;
                    case "slide-left":
                        a._totalNews <= 1 ? a._ul.find("li").animate({
                            left: "50%",
                            opacity: 0
                        }, 300, function () {
                            p(this).css({
                                left: -50,
                                opacity: 0,
                                display: "block"
                            }), p(this).animate({
                                left: 0,
                                opacity: 1
                            }, 300)
                        }) : (a._ul.find("li:visible").animate({
                            left: "50%",
                            opacity: 0
                        }, 300, function () {
                            p(this).hide()
                        }), a._ul.find("li").eq(a._activeNews).css({
                            left: -50,
                            opacity: 0
                        }).show(), a._ul.find("li").eq(a._activeNews).animate({
                            left: 0,
                            opacity: 1
                        }, 300));
                        break;
                    case "slide-right":
                        a._totalNews <= 1 ? a._ul.find("li").animate({
                            left: "-50%",
                            opacity: 0
                        }, 300, function () {
                            p(this).css({
                                left: "50%",
                                opacity: 0,
                                display: "block"
                            }), p(this).animate({
                                left: 0,
                                opacity: 1
                            }, 300)
                        }) : (a._ul.find("li:visible").animate({
                            left: "-50%",
                            opacity: 0
                        }, 300, function () {
                            p(this).hide()
                        }), a._ul.find("li").eq(a._activeNews).css({
                            left: "50%",
                            opacity: 0
                        }).show(), a._ul.find("li").eq(a._activeNews).animate({
                            left: 0,
                            opacity: 1
                        }, 300));
                        break;
                    default:
                        a._ul.find("li").hide(), a._ul.find("li").eq(a._activeNews).show()
                }
            },
            f = function () {
                if (a._pause = !1, a.settings.play) switch (a.settings.effect) {
                    case "scroll":
                        "rtl" === a.settings.direction ? a._ul.width() > a._news.width() ? r() : a._ul.css({
                            marginRight: 0
                        }) : a._ul.width() > a._news.width() ? l() : a._ul.css({
                            marginLeft: 0
                        });
                        break;
                    default:
                        a.pause(), a._interval = setInterval(function () {
                            a.next()
                        }, a.settings.delayTimer)
                }
            },
            _ = function () {
                a._element.width() < 480 ? (a._label.hide(), "rtl" == a.settings.direction ? a._news.css({
                    right: 0
                }) : a._news.css({
                    left: 0
                })) : (a._label.show(), "rtl" == a.settings.direction ? a._news.css({
                    right: a._label.outerWidth()
                }) : a._news.css({
                    left: a._label.outerWidth()
                }))
            };
        a.init = function () {
            if (a.settings = p.extend({}, i, t), "fixed-top" === a.settings.position ? a._element.addClass("bn-fixed-top").css({
                    "z-index": a.settings.zIndex
                }) : "fixed-bottom" === a.settings.position && a._element.addClass("bn-fixed-bottom").css({
                    "z-index": a.settings.zIndex
                }), "default" != a.settings.fontSize && a._element.css({
                    "font-size": a.settings.fontSize
                }), "default" != a.settings.themeColor && (a._element.css({
                    "border-color": a.settings.themeColor,
                    color: a.settings.themeColor
                }), a._label.css({
                    background: a.settings.themeColor
                })), "default" != a.settings.background && a._element.css({
                    background: a.settings.background
                }), a._element.css({
                    height: a.settings.height,
                    "line-height": a.settings.height - 2 * a.settings.borderWidth + "px",
                    "border-radius": a.settings.radius,
                    "border-width": a.settings.borderWidth
                }), a._li.find(".bn-seperator").css({
                    height: a.settings.height - 2 * a.settings.borderWidth
                }), a._element.addClass("bn-effect-" + a.settings.effect + " bn-direction-" + a.settings.direction), o(), "object" == typeof a.settings.source) switch (a.settings.source.type) {
                case "rss":
                    "rss2json" === a.settings.source.usingApi ? (s(), 0 < a.settings.source.refreshTime && setInterval(function () {
                        a._activeNews = 0, a.pause(), a._ul.empty().append('<li style="display:block; padding-left:10px;"><span class="bn-loader-text">......</span></li>'), setTimeout(function () {
                            s()
                        }, 1e3)
                    }, 1e3 * a.settings.source.refreshTime * 60)) : ((l = new XMLHttpRequest).open("GET", "https://query.yahooapis.com/v1/public/yql?q=" + encodeURIComponent('select * from rss where url="' + a.settings.source.url + '" limit ' + a.settings.source.limit) + "&format=json", !0), l.onreadystatechange = function () {
                        if (4 == l.readyState)
                            if (200 == l.status) {
                                var e = JSON.parse(l.responseText),
                                    t = "",
                                    i = "";
                                switch (a.settings.source.showingField) {
                                    case "title":
                                        i = "title";
                                        break;
                                    case "description":
                                        i = "description";
                                        break;
                                    case "link":
                                        i = "link";
                                        break;
                                    default:
                                        i = "title"
                                }
                                var s = "";
                                "undefined" != a.settings.source.seperator && void 0 !== a.settings.source.seperator && (s = a.settings.source.seperator);
                                for (var n = 0; n < e.query.results.item.length; n++) a.settings.source.linkEnabled ? t += '<li><a target="' + a.settings.source.target + '" href="' + e.query.results.item[n].link + '">' + s + e.query.results.item[n][i] + "</a></li>" : t += "<li><a>" + s + e.query.results.item[n][i] + "</a></li>";
                                a._ul.empty().append(t), a._li = a._ul.children("li"), a._totalNews = a._ul.children("li").length, o(), "scroll" != a.settings.effect && d(), a._li.find(".bn-seperator").css({
                                    height: a.settings.height - 2 * a.settings.borderWidth
                                }), f()
                            } else a._ul.empty().append('<li><span class="bn-loader-text">' + a.settings.source.errorMsg + "</span></li>")
                    }, l.send(null));
                    break;
                case "json":
                    n(), 0 < a.settings.source.refreshTime && setInterval(function () {
                        a._activeNews = 0, a.pause(), a._ul.empty().append('<li style="display:block; padding-left:10px;"><span class="bn-loader-text">......</span></li>'), setTimeout(function () {
                            n()
                        }, 1e3)
                    }, 1e3 * a.settings.source.refreshTime * 60);
                    break;
                default:
                    console.log('Please check your "source" object parameter. Incorrect Value')
            } else "html" === a.settings.source ? ("scroll" != a.settings.effect && d(), f()) : console.log('Please check your "source" parameter. Incorrect Value');
            var l;
            a.settings.play ? a._action.find("span").removeClass("bn-play").addClass("bn-pause") : a._action.find("span").removeClass("bn-pause").addClass("bn-play"), a._element.on("mouseleave", function (e) {
                var t = p(document.elementFromPoint(e.clientX, e.clientY)).parents(".bn-breaking-news")[0];
                p(this)[0] !== t && (!0 === a.settings.stopOnHover ? !0 === a.settings.play && a.play() : !0 === a.settings.play && !0 === a._pause && a.play())
            }), a._element.on("mouseenter", function () {
                !0 === a.settings.stopOnHover && a.pause()
            }), a._next.on("click", function () {
                a._controlsIsActive && (a._controlsIsActive = !1, a.pause(), a.next())
            }), a._prev.on("click", function () {
                a._controlsIsActive && (a._controlsIsActive = !1, a.pause(), a.prev())
            }), a._action.on("click", function () {
                a._controlsIsActive && (a._action.find("span").hasClass("bn-pause") ? (a._action.find("span").removeClass("bn-pause").addClass("bn-play"), a.stop()) : (a.settings.play = !0, a._action.find("span").removeClass("bn-play").addClass("bn-pause")))
            }), _(), p(window).on("resize", function () {
                _(), a.pause(), a.play()
            })
        }, a.pause = function () {
            a._pause = !0, clearInterval(a._interval), cancelAnimationFrame(a._frameId)
        }, a.stop = function () {
            a._pause = !0, a.settings.play = !1
        }, a.play = function () {
            f()
        }, a.next = function () {
            ! function () {
                switch (a.settings.effect) {
                    case "scroll":
                        c();
                        break;
                    default:
                        a._activeNews++, a._activeNews >= a._totalNews && (a._activeNews = 0), d()
                }
            }()
        }, a.prev = function () {
            ! function () {
                switch (a.settings.effect) {
                    case "scroll":
                        u();
                        break;
                    default:
                        a._activeNews--, a._activeNews < 0 && (a._activeNews = a._totalNews - 1), d()
                }
            }()
        }, a.init()
    }, p.fn.breakingNews = function (t) {
        return this.each(function () {
            if (null == p(this).data("breakingNews")) {
                var e = new p.breakingNews(this, t);
                p(this).data("breakingNews", e)
            }
        })
    }
}(jQuery);



