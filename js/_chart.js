// $scope.chartConfig.series[0].visible = false;
try{

  
  
  
  
  
  
  
  
  
  
  
  
  
var jsobject = {
  data: [
    110918014828.58,
    123528752302.25,
    132862436591.28,
    155476445448.28,
    161679441287.24,
    158528181698.21,
    164066857164.88,
    164394920835.42,
    170635522619.67,
    198001509593.71,
    193508008534.69,
    179598527860.35,
    145290931115.95,
  ],
  data2: [
    2087008757435.94,
2185699113091.03,
2000248221526.30,
2081078272266.13,
2057214816577.47,
1902408703619.21,
1908447533399.00,
1818765588633.12,
1844302964673.90,
9025438669340.85,
8748845573208.70,
8425255907997.67,
7561968365701.89
  ],
  label: [
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    "Aramco's<br>Listing",
    null,
    null
  ],
  prctg: [
  ]
  // ] : 
}

var jsobject2 = {
  data: [
    556,
    667,
    772,
    882,
    1130,
    1214,
    1305,
    1395,
    1458,
    1547,
    1716,
    1853,
    1939
  ],
  label: [
    "",
    "MT30 Index</br> Go Live",
    "FTSE <sup>1st</sup></br>Tranche",
    "FTSE <sup>2nd</sup></br> Tranche",
    null,
    "MSCI <sup>1st</sup></br> Tranche",
    "FTSE <sup>3rd</sup></br> Tranche",
    "MSCI <sup>1st</sup></br> Tranche",
    null,
    "FTSE <sup>4th</sup></br> Tranche",
    null,
    "Aramco's </br>Listings",
    null
  ]

}



let sum =jsobject.data.reduce((a, b) => a + b, 0);

jsobject.data.forEach(function(x,i){
  // jsobject.prctg.push((prctg));
  // console.log(jsobject2.data[1])
  // console.log(x)

  let prctg = (x/jsobject.data2[i])*100;

  jsobject.prctg.push((prctg));

});
// console.log( Math.max( ...jsobject.prctg))
Highcharts.setOptions({
  lang: {
    thousandsSep: ',',
    numericSymbols: ['K', 'M','B','T']
  },
  chartIntervals: 10,

});
// debugger;
var chart1x = Highcharts.chart('container1', {
  plotOptions: {
    series: {
      states: {
        inactive: {
          opacity: 1
        },
      }
    }
  },
  exporting: false,
  lang: {
    thousandsSep: ','
  },

  chart: {
    zoomType: false
  },
  title: {
    text: ''
  },

  xAxis: [{

    categories: ['Mar-19','Apr-19','May-19','Jun-19','Jul-19','Aug-19','Sep-19','Oct-19','Nov-19','Dec-19','Jan-19','Feb-19','Mar-20'],
    tooltip: {
      crosshairs: false
    },
    labels: {
      align: 'center',
      useHTML: true,
      style: {
        fontSize: '13px',
        paddingTop: '30px',
      }
    }
  }],
  yAxis: [

    { // Primary yAxis
      // min: 0,
      // max: 100,
      min: 0,
      max: Math.max( ...jsobject.prctg),
      tickInterval:(Math.max( ...jsobject.prctg)+1)/5,
      labels: {
        
        format: '{value:,.0f}%',
        style: {
          color: "#7dad14 !important"
        }
      },
      title: {
        text: '% of Total Market',
        style: {
          color: "#7dad14 !important"
        },
        formatter: function() {
          // console.log(100*jsobject2.data.slice(-1).pop()/jsobject2.data.reduce((a, b) => a + b, 0))
          return this.value+"%xx";
        // var percent = (100 * this.y) / (this.series.data[0].y + this.series.data[1].y + this.series.data[2].y);
        // percent = percent.toFixed(1);
        // return 'value: ' + this.y + ', percent: ' + percent + '%';
      }
      },
      opposite: true,

      
    }, { // Secondary yAxis

      title: {
        text: 'QFI Ownership',
        style: {
          color: "rgb(0, 141, 216) !important"
        }
      },
      labels: {
         
        // format: '{value:,.0f}',
        style: {
          // color: Highcharts.getOptions().colors[2]
          color: "rgb(0, 141, 216) !important"
        }
      },
    }
  ],

  tooltip: {
    shared: true,
    crosshairs: false,
    pointFormatter: function() {
      var point = this;
      var num;
      if(point.color == "#628ca7"){
        num =  numberWithCommas(point.options.y);
      }else{
        num = (point.options.y).toFixed(2)+"%";
      }
      return '<span style="color:' + point.color + '">\u25CF </span><b>' + point.series.name + ': '+num+'</b><br/>';
    }
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 100,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'QFI Ownership',
    showInLegend: false,
    type: 'column',
    yAxis: 1,

    data: jsobject.data,
    dataLabels: {
      enabled: true,
      inside: true,
      useHTML: true,
      formatter: function () {
        if (this.y > 0)
          if (jsobject.label[this.series.data.indexOf(this.point)])
            return (jsobject.label[this.series.data.indexOf(this.point)]);
      },
      verticalAlign: 'top'
    },
    color: "#628ca7",
  }, {
    name: '% of Total Market',
    type: 'spline',
    showInLegend: false,
    data: jsobject.prctg,

    color: Highcharts.getOptions().colors[2]

  }]
}, function (chart) {

  var max = chart.yAxis[0].toPixels(chart.series[0].data[0].y, true);
  // console.log(max)
  $.each(chart.series[0].data, function (i, d) {
    try {
      d.dataLabel.attr({
        y: d.dataLabel.y - 40
      });
    } catch (error) {
      // console.log(error)
    }

    // console.log(d)
  });
});


// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************

















var chart2x = Highcharts.chart('container2', {
  plotOptions: {
    series: {
      states: {
        inactive: {
          opacity: 1
        }
      }
    }
  },
  exporting: false,
  lang: {
    thousandsSep: ','
  },

  chart: {
    type: 'column',
    zoomType: false

  },
  title: {
    text: ''
  },

  xAxis: [{
    categories: [
      "Jan-19",
      "Feb-19",
      "Mar-19",
      "Apr-19",
      "May-19",
      "Jun-19",
      "Jul-19",
      "Aug-19",
      "Sep-19",
      "Oct-19",
      "Nov-19",
      "Dec-19",
      "Jan-20",
    ],
    tooltip: {
      crosshairs: false
    },
    labels: {
      align: 'center',
      useHTML: true,
      // rotation: -45,
      autoRotation: [0, -45],
      // align:'bottom',
      style: {
        fontSize: '13px',
        paddingTop: '30px',
      }
    }
  }],
  yAxis: [

    // { // Primary yAxis
    //   min: 0,
    //   max: Math.round(100*jsobject2.data.slice(-1).pop()/jsobject2.data.reduce((a, b) => a + b, 0))+3,
    //   tickInterval: Math.round(100*jsobject2.data.slice(-1).pop()/jsobject2.data.reduce((a, b) => a + b, 0)+3)/5,
    //   labels: {
    //     format: '',
    //     style: {
    //       color: "rgb(0, 141, 216) !important"
    //     },
    //     formatter: function() {
    //         let total = jsobject2.data.reduce((a, b) => a + b, 0);
    //         // console.log(100*jsobject2.data.slice(-1).pop()/jsobject2.data.reduce((a, b) => a + b, 0))
    //         return this.value+"%";
    //       // var percent = (100 * this.y) / (this.series.data[0].y + this.series.data[1].y + this.series.data[2].y);
    //       // percent = percent.toFixed(1);
    //       // return 'value: ' + this.y + ', percent: ' + percent + '%';
    //     }
    //   },
    //   title: {
    //     text: '% of Total Registered QFI',
    //     style: {
    //       color: "rgb(0, 141, 216) !important"
    //     }
    //   },
    //   opposite: true,        
    // },
    { // Secondary yAxis
      
      title: {
        text: 'Number of Registered QFI',
        style: {
          color: "rgb(0, 141, 216) !important"
        }
      },
      labels: {
        // format: '{value:,.0f}',
        style: {
          color: "rgb(0, 141, 216) !important"
        }
      } 
    }
  ],

  tooltip: {
    crosshairs: false,

      // borderWidth: 0,
      // backgroundColor: "rgba(255,255,255,0)",
      // borderRadius: 0,
      // shadow: false,
      // useHTML: true,
      // formatter: function () {
      //   // return this;
      //   let percentX = (100*this.y/jsobject2.data.reduce((a, b) => a + b, 0)).toFixed(2);
      //   return '<div class="tooltip"><b>' + this.x + '<br />'  + 'Registered QFI: '+this.y+'<br/>' + 'Registered QFI in %: '+percentX+'%</div>';
      // },
      positioner: function(labelWidth, labelHeight, point) {
        var x;
        if(labelWidth > point.plotX){
          x = point.plotX;
        }else{
          x = point.plotX -100;
        }

        var tooltipX = x;
        var tooltipY = point.plotY+40;
        return {
            x: tooltipX,
            y: tooltipY
        };
    }
  },
 
  series: [{
    name: 'Number of Registered QFI',
    // visible: false,
    // showInLegend: "false"
    showInLegend: false,
    type: 'column',
    yAxis: 0,

    data: jsobject2.data,
    dataLabels: {
      enabled: true,
      allowOverlap:false,
      align: 'center',
      color: '#FFFFFF',
      overflow:"none",
      inside: true,
      crop: false,
      useHTML: true,

      formatter: function () {
        
        if (this.y > 0)
          if (jsobject2.label[this.series.data.indexOf(this.point)])
            return jsobject2.label[this.series.data.indexOf(this.point)];
      },
      verticalAlign: 'top'

    },
    color: "#628ca7",


  }]
}, function (chart) {
  $.each(chart.series[0].data, function (i, d) {
    try {
      d.dataLabel.attr({
        y: d.dataLabel.y - 40
      });
    } catch (error) {
      // console.log(error)
    }
  });
});



}catch{
  
}
$(window).resize();
function numberWithCommas(x) {
  x = parseInt(x)
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}